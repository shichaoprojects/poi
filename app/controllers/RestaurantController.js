var restaurantService = require('../services/RestaurantService');
var resposneHandler = require('../middlewares/response-handler');

module.exports = function (router) {
  router.get('/restaurants/:id', function(req, res) {
    restaurantService.getRestaurantById(req.params.id, function(err, msg, data) {
      resposneHandler(res, err, msg, data);
    });
  });

  router.get('/restaurants', function(req, res) {
    var filters = {};

    if(req.query.center != null) {
      filters.center = req.query.center;
    }

    if(req.query.radius != null) {
      filters.radius = Number(req.query.radius);
    }

    if(req.query.limit != null) {
      filters.limit = Number(req.query.limit);
    } else {
      filters.limit = 10;
    }

    if(req.query.queueTimeLessThan != null && req.query.queueTimeLessThan != 'undefined') {
      filters.queueTimeLessThan = Number(req.query.queueTimeLessThan);
    }

    restaurantService.getRestaurants(req.params.id, req.query, function(err, msg, data) {
      resposneHandler(res, err, msg, data);
    });
  });

  router.post('/restaurants', function(req, res) {
    restaurantService.addRestaurant(req.body, function(err, msg, data) {
      resposneHandler(res, err, msg, data);
    });
  });

  return router;
}
