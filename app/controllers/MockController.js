var resposneHandler = require('../middlewares/response-handler');
var Chance = require('chance');
var chance = new Chance();

module.exports = function (router) {
  // mock the API to return the queue time for the restaurant
  // both the queue time and the last update time are generated randomly
  router.get('/queueTime/:id', function(req, res) {
    var randomQeueTime = chance.integer({min: 0, max: 20});

    var date = new Date();
    var currentTimestamp = date.getTime();
    var randomInterval = chance.integer({min: 0, max: 10 * 60 * 1000})
    var randomLastUpdateTimestamp = currentTimestamp - randomInterval;

    var data = {
      "lastUpdateTime" : new Date(randomLastUpdateTimestamp).toLocaleString(),
      "queueTime" : Math.round(randomQeueTime),
      "queueTimeUnit": "minute"
    };

    resposneHandler(res, null, null, data);
  });

  // mock the API to return the list of Koff trams and their coordinates
  // the coordinates are generated randomly
  router.get('/koffTrams/', function(req, res) {
    var data = [];

    for (var i = 1; i <= 10; i++) {
      var coordinatesStr = chance.coordinates();
      var coordinatesArr = coordinatesStr.split(',');
      var lat = Number(coordinatesArr[1].trim());
      var long = Number(coordinatesArr[0].trim());
      var coordinates = [ long , lat ];
      
      data.push({
        "name": "tram_" + i,
        "coordinates" : coordinates
      });
    }

    resposneHandler(res, null, null, data);
  });

  return router;
}
