var dynamicPoiService = require('../services/DynamicPoiService');
var resposneHandler = require('../middlewares/response-handler');

module.exports = function (router) {
  router.get('/dynamicPoi/koffTrams/nearest', function(req, res) {
    var filters = {};

    if(req.query.center != null) {
      filters.center = req.query.center;
    } else {
      resposneHandler(res, "missing center parameter 'center'", null, null);
    }

    dynamicPoiService.getKoffTrams(filters, function(err, msg, data) {
      resposneHandler(res, err, msg, data);
    });
  });

  router.get('/dynamicPoi/koffTrams', function(req, res) {    
    dynamicPoiService.getKoffTrams({}, function(err, msg, data) {
      resposneHandler(res, err, msg, data);
    });
  });

  return router;
}
