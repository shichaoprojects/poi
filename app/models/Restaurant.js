var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var restaurantSchema = new Schema({
  name : {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  integrationId: {
    type: String,
    required: true
  },
  queueTime: {
    type: Number,
    required: false
  },
  queueTimeLastUpdateTime: {
    type: Date,
    required: false
  },
  location: {
    coordinates : [
      {
        type: Number,
        required: true
      },
      {
        type: Number,
        required: true
      }
    ],
    address : {
      street: {
        type : String,
        required: false
      },
      city: {
        type : String,
        required: false
      },
      country: {
        type : String,
        required: false
      }
    },
    floor: {
      type: String,
      required: false
    },
    doorNumber: {
      type: String,
      required: false
    }
  }
});

// add virtual field to describe the last update time for the queue time
restaurantSchema
.virtual('queueTimeLastUpdateDesc')
.get(function () {
  if(!this.queueTimeLastUpdateTime || this.queueTimeLastUpdateTime == null) {
    return 'not available';
  } else {
    var diff = Math.abs(new Date() - new Date(this.queueTimeLastUpdateTime));
    var minutes = Math.floor( ( diff / 1000 ) / 60 );
    return 'updated ' + minutes + ' minute(s) ago';
  }
});

// add virtual field to describe the the queue time unit
restaurantSchema
.virtual('queueTimeUnit')
.get(function () {
  return 'minutes';
});

// for the geometry feature in MongoDB
restaurantSchema.index({ "location.coordinates": '2dsphere' });

// define the unique key
restaurantSchema.index({ name: 1, location: 1 }, { unique: true });

// convert the DB object format to the format that is returned in the API response
restaurantSchema.options.toJSON = {
  transform: function(doc, ret) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
  virtuals: true
};

module.exports = mongoose.model('Restaurant', restaurantSchema);;
