var http = require('http');
var async = require('async');
var geolib = require('geolib')

// function to find Koff trams with filters supported
exports.getKoffTrams = function(queryParams, controllerCallback) {
  var koffTrams;

  async.series([
    function(asyncCallback) {
      var request = require('request');
      username = "poi",
      password = "poi",
      url = "http://127.0.0.1:3000/mock/koffTrams",
      auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

      request({
        url : url,
        timeout : 1500,
        headers : { "Authorization" : auth }
      }, function (err, res, body) {
        if (err) {
          controllerCallback(err, null, null);
        } else {
          responseData = JSON.parse(body);

          koffTrams = responseData;
          asyncCallback();
        }
      });
    },

    function(asyncCallback) {
      if(queryParams.center) {
        var longLat = queryParams.center.split(',');
        for (var i =0; i < longLat.length; i++) {
          longLat[i] = longLat[i].trim();
        }

        var long = Number(longLat[0]);
        var lat = Number(longLat[1]);

        var locations = {};
        var length = koffTrams.length;
        for (var i = 0; i < length; i++) {
          koffTram = koffTrams[i];

          locations[koffTram.name] = {
              "latitude" : koffTram.coordinates[1],
              "longitude" : koffTram.coordinates[0]
          };
        }

        var nearest = geolib.findNearest({
          latitude : lat,
          longitude : long
        }, locations, 1);

        nearest.distance = Math.round(nearest.distance / 1000) + ' km';
        nearest.name = nearest.key;
        delete nearest.key;
        nearest.coordinates = [ nearest.longitude,nearest.latitude ];
        delete nearest.longitude;
        delete nearest.latitude;
        controllerCallback(null, null, nearest);
      } else {
        controllerCallback(null, null, koffTrams);
      }
    }
  ]);
};
