var Restaurant = require('../models/Restaurant');
var http = require('http');
var async = require('async');

// function to add restaurant, the queue time is fetched and updated
// to keep it simple, the queue time is fetched from the third party only when the restaurant is added
// ideally, the queue time can be updated by the third party through API (push)
// or can be fetched and updated intervally by using other service (pull)
exports.addRestaurant = function(reqObj, controllerCallback) {
  var newRestaurant = new Restaurant();

  async.series([
    function(asyncCallback) {
      Restaurant.findOne({
        "name": reqObj.name,
        "location": reqObj.location
      })
      .select("_id")
      .exec(function (err, restaurant) {
        if (err) {
          controllerCallback(err);
        } else if(restaurant != null) {
          controllerCallback(null, "restaurant exists already.", null);
        } else {
          newRestaurant.name = reqObj.name;
          newRestaurant.description = reqObj.description;
          newRestaurant.integrationId = reqObj.integrationId;
          newRestaurant.location = reqObj.location;

          asyncCallback();
        }
      })
    },

    function(asyncCallback) {
      var request = require('request');
      username = "poi",
      password = "poi",
      url = "http://127.0.0.1:3000/mock/queueTime/" + reqObj.integrationId,
      auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

      request({
        url : url,
        timeout : 1500,
        headers : { "Authorization" : auth }
      }, function(err, response, body) {
        if (err) {
          console.log("failed to fetch queue time for restaurant with integration id: " + reqObj.integrationId);
        } else {
          responseData = JSON.parse(body);
          newRestaurant.queueTime = responseData.queueTime;
          newRestaurant.queueTimeLastUpdateTime = responseData.lastUpdateTime;
        }

        newRestaurant.save(function(err, savedRestaurant) {
          if (err) {
            controllerCallback(err);
          } else {
            controllerCallback(null, "restaurant added successfully!", null);
          }
        });
      })
    }
  ]);
}

// function to find restaurant by using DB ID
exports.getRestaurantById = function(id, controllerCallback) {
  Restaurant.findOne(
    { "_id": id }
  )
  .exec(function(err, restaurant) {
    if (err) {
      controllerCallback(err);
    } else {
      restaurantObj = restaurant;
      controllerCallback(null, null, restaurantObj);
    }
  });
}

// function to find restaurants with filters
exports.getRestaurants = function(id, queryParams, controllerCallback) {
  if(queryParams.center && queryParams.radius) {
    var longLat = queryParams.center.split(',');
    for (var i =0; i < longLat.length; i++) {
      longLat[i] = longLat[i].trim();
    }

    var long = Number(longLat[0]);
    var lat = Number(longLat[1]);

    var query = {
      "location.coordinates": {
        $near: {
          $geometry : {
            type : "Point",
            coordinates: [ long, lat ]
          },
          $maxDistance: queryParams.radius
        }
      }
    };
  } else {
    var query = {};
  }

  var queryBuilder = Restaurant.find(query);
  if(queryParams.queueTimeLessThan) {
    queryBuilder = queryBuilder.where('queueTime').lte(queryParams.queueTimeLessThan);
  }
  queryBuilder
  .limit(Number(queryParams.limit))
  .exec(function(err, restaurants) {
    if (err) {
      controllerCallback(err);
    } else {
      controllerCallback(null, null, restaurants);
    }
  });
}
