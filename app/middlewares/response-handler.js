module.exports = function(res, err, msg, payload) {
  var responseData = {}
  if (err) {
    responseData.result = 'KO';
    responseData.message = err;
    res.status(400).send(responseData);
  } else {
    if(payload != null) {
      responseData = payload;
    } else {
      responseData.result = 'OK';

      if(msg != null) {
        responseData.message = msg;
      }
    }

    res.status(200).send(responseData);
  }
}
