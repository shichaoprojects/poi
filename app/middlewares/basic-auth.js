var resposneHandler = require('./response-handler');

module.exports = function(req, res, next) {
    var auth;

    if (req.headers.authorization) {
      auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
    }

    if (!auth || auth[0] !== 'poi' || auth[1] !== 'poi') {
				resposneHandler(res, 'Unauthorized', null, null);
    } else {
        next();
    }
}
