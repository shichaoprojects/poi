var express = require('express');
var router = express.Router();
var resposneHandler = require('../middlewares/response-handler');

router.use('/restaurants', require('../controllers/RestaurantController')(router));
router.use('/dynamicPoi', require('../controllers/DynamicPoiController')(router));

// simple mock component to mock external API
router.use('/mock', require('../controllers/MockController')(router));

// handle the rest of the endpoints
router.use('/', function(req, res) {
  resposneHandler(res, "invalid request, unsupported endpoint.", null, null);
});

module.exports = router;
