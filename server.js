var express = require('express');
var app = express();
var http = require('http').Server(app);
var cors = require('cors');
var router = require('./app/routes/routes');
var bodyParser = require('body-parser');
var contentType = require('./app/middlewares/content-type');
var basicAuth = require('./app/middlewares/basic-auth');
var mongoose = require('mongoose');

// enable cross-region call
app.use(cors());
// serve the swagger-editor without using router
app.use('/swagger-editor', express.static('./node_modules/swagger-editor'));

// handle the content type in the API response
app.use(contentType);
// add the HTTP basic authentication
app.use(basicAuth);
// parse API response data to JSON format
app.use(bodyParser.json());

// connect to the database
mongoose.connect('mongodb://localhost/poi');

// specify which router to use
app.use(router);

http.listen(3000, function() {
	console.log('Express server listening on port 3000');
});
